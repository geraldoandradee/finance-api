from django.db import models
from djmoney.models.fields import MoneyField
from financeapi.lib import BaseModel


class Card(BaseModel):
    class Meta:
        ordering = ('name',)

    CHOICES = (
        ('mastercard', 'Mastercard'),
        ('visa', 'Visa')
    )
    CARD_TYPE = (
        ('debit', 'Debit'),
        ('credit', 'Credit'),
        ('both', 'Credit / Debit'),
    )
    name = models.CharField(max_length=250, null=False, blank=False)
    flag = models.CharField(max_length=20, choices=CHOICES, null=False, blank=False)
    card_type = models.CharField(max_length=20, choices=CARD_TYPE, null=False, blank=False)


class Transaction(BaseModel):
    name = models.CharField(max_length=250, null=True, blank=True)
    value = MoneyField(max_digits=14, decimal_places=2, default_currency='EUR', db_column='value', default=0.00)
    payed_at = models.DateTimeField(null=True, default=None, blank=False)
    due_at = models.DateTimeField(null=True, default=None, blank=False)
