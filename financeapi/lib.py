from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(db_column='id', primary_key=True)
    created_at = models.DateTimeField(auto_created=True, db_column='created_at')
    updated_at = models.DateTimeField(auto_created=True, db_column='updated_at', blank=True, null=True, default=None)
    deleted_at = models.DateTimeField(auto_created=True, db_column='deleted_at', blank=True, null=True, default=None)
